package get

import (
	"os"
	"os/exec"
)

// Cmd runs a command
func Cmd(name string, args ...string) error {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
