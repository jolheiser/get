package get

// Expand is the same as Copy, but expands as the copy happens
func Expand(base, from, to string) error {
	return copyExpand(base, from, to, true)
}
