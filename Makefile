GO ?= go

.PHONY: build
build:
	$(GO) build go.jolheiser.com/get/cmd/get

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test -race ./...
