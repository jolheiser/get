package get

import (
	"io/ioutil"
	"net/http"
	"os"
)

// Fetch gets an asset via HTTP
func Fetch(from, to string) error {
	resp, err := http.Get(from)
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fi, err := os.Create(to)
	if err != nil {
		return err
	}
	defer fi.Close()

	if _, err := fi.Write(body); err != nil {
		return err
	}

	return nil
}
