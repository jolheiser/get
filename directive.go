package get

import (
	"fmt"
	"strings"
)

type Type string

const (
	COPY   Type = "COPY"
	ECHO   Type = "ECHO"
	GET    Type = "GET"
	CMD    Type = "CMD"
	FETCH  Type = "FETCH"
	EXPAND Type = "EXPAND"
)

type Directive struct {
	Type       Type     `json:"type"`
	Args       []string `json:"args"`
	Comments   []string `json:"comments"`
	ParseError error    `json:"parse_error,omitempty"`
}

func ParseType(directive string) (Type, error) {
	switch strings.ToUpper(directive) {
	case "COPY":
		return COPY, nil
	case "ECHO":
		return ECHO, nil
	case "GET":
		return GET, nil
	case "CMD":
		return CMD, nil
	case "FETCH":
		return FETCH, nil
	case "EXPAND":
		return EXPAND, nil
	default:
		return "", ErrInvalidDirective{directive}
	}
}

type ErrInvalidDirective struct {
	Directive string
}

func (e ErrInvalidDirective) Error() string {
	return fmt.Sprintf("invalid directive: %s", e.Directive)
}
