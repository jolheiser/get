package get

import (
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
)

// Get parses and runs a Getfile
func Get(arg string) error {
	tmp, err := ioutil.TempDir(os.TempDir(), "get")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmp)

	if err := get(tmp, arg); err != nil {
		return err
	}

	return nil
}

func get(tmp, arg string) error {
	u, err := url.Parse(arg)
	if err != nil {
		return err
	}

	base := filepath.Base(u.Path)

	var parsed *AST
	if u.Scheme == "" {
		// Local Getfile
		base = filepath.Dir(arg)
		parsed, err = ParseFile(arg)
		if err != nil {
			return err
		}
	} else {
		// Remote Getfile
		base = filepath.Join(tmp, base)
		parsed, err = ParseRepo(base, u.String())
		if err != nil {
			return err
		}
	}

	for _, d := range parsed.Directives {
		switch d.Type {
		case CMD:
			err = Cmd(d.Args[0], d.Args[1:]...)
		case COPY:
			err = Copy(base, d.Args[0], d.Args[1])
		case ECHO:
			err = Echo(d.Args...)
		case EXPAND:
			err = Expand(base, d.Args[0], d.Args[1])
		case FETCH:
			err = Fetch(d.Args[0], d.Args[1])
		case GET:
			err = get(tmp, d.Args[0])
		}
		if err != nil {
			return err
		}
	}
	return nil
}
