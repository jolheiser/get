package get

import "fmt"

// Echo echoes a message
func Echo(args ...string) error {
	i := make([]interface{}, len(args))
	for idx, a := range args {
		i[idx] = a
	}
	fmt.Println(i...)
	return nil
}
