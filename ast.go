package get

import (
	"encoding/json"
)

type AST struct {
	Directives []Directive `json:"directives"`
}

func (a *AST) String() string {
	out, _ := json.MarshalIndent(a, "", "\t")
	return string(out)
}
