package main

import (
	"flag"
	"fmt"
	"go.jolheiser.com/get"
	"io/ioutil"
	"net/url"
	"os"
)

var (
	jsonFlag bool
)

func main() {
	flag.BoolVar(&jsonFlag, "json", false, "Output the Getfile as JSON instead of executing")
	flag.Parse()

	target := "Getfile"
	if flag.NArg() > 0 {
		target = flag.Arg(0)
	}

	if jsonFlag {
		JSON(target)
		return
	}

	if err := get.Get(target); err != nil {
		fmt.Println(err)
	}
}

func JSON(target string) {
	tmp, err := ioutil.TempDir(os.TempDir(), "get")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tmp)

	u, err := url.Parse(target)
	if err != nil {
		panic(err)
	}

	var parsed *get.AST
	if u.Scheme == "" {
		// Local Getfile
		parsed, err = get.ParseFile(target)
		if err != nil {
			panic(err)
		}
	} else {
		// Remote Getfile
		parsed, err = get.ParseRepo(tmp, u.String())
		if err != nil {
			panic(err)
		}
	}

	fmt.Println(parsed)
}
