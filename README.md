# Get

[Getfile example](Getfile)

`get https://gitea.com/jolheiser/get`

or

`get path/to/Getfile`

## License

[MIT](LICENSE)